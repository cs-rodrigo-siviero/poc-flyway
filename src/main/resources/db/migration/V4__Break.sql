-- PL/SQL block
CREATE TRIGGER test_trig2 AFTER insert ON test_user
BEGIN
       UPDATE test_user SET name = CONCAT(name, 'Rollback');
END;
/
